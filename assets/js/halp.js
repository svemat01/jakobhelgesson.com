var token
var members
var host = "https://halper.halpgta.com"

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function removeCookie(cname) {
    setCookie(cname,"", 0)
    token = prompt("Please enter your API token");
    TestAuth()
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkToken() {
    token = getCookie("token");
    if (token != "") {
        TestAuth();
    } else {
        token = prompt("Please enter your API token");
        TestAuth()
    }
}

function GetMembers(){
    var url = host + "/api/members";

    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);

    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Authorization", `Bearer ${token}`);

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(xhr.status);
            if (xhr.status === 401) {
                authFailed()
                return
            }
            console.log(xhr.responseText)
            members = JSON.parse(xhr.responseText)
            console.log(members)
            AddOptions()
        }};

    xhr.send();
}

function AddOptions(){
    var x = document.getElementById("MembersSelect");
    
    for (const [key, value] of Object.entries(members)) {
        var option = document.createElement("option");
        option.text = key;
        x.add(option);
        console.log(key, value);
    }
}

function GetMemberData(){
    document.getElementById("loader").classList.remove("opacity-0")
    var SelectObj = document.getElementById("MembersSelect");
    id = members[SelectObj.value].toString()
    console.log(id) 

    var url = host + "/api/member_full";

    var xhr = new XMLHttpRequest();
    var FD  = new FormData();
    FD.append("discord_id", id)
    xhr.open("POST", url);

    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Authorization", `Bearer ${token}`);

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(xhr.status);
            if (xhr.status === 401) {
                authFailed()
                return
            }
            member_data = JSON.parse(xhr.responseText)
            console.log(member_data)
            FillData(member_data)
        }};
    xhr.send(FD);
}

function FillData(member_data) {
    discord_data = member_data.Discord
    roster_data = member_data.Roster
    app_data = member_data.Application
    document.getElementById("DiscordSubData").classList.remove("hidden")
    document.getElementById("RosterSubData").classList.remove("hidden")
    document.getElementById("AppSubData").classList.remove("hidden")

    console.log(discord_data)
    if (discord_data.Found == true){
        document.getElementById("DFound").innerHTML = "True"
        document.getElementById("DAvatar").src = discord_data.AvatarUrl
        username_ele = document.getElementById("DName")
        username_ele.innerHTML = discord_data.Name
        username_ele.style.color = discord_data.Color
        document.getElementById("DGName").innerHTML = discord_data.GlobalName
        document.getElementById("DCreatedAt").innerHTML = discord_data.CreatedAt
        document.getElementById("DJoinedAt").innerHTML = discord_data.JoinedAt
        document.getElementById("DTRole").innerHTML = discord_data.TopRole
        document.getElementById("DStats").innerHTML = discord_data.Status
    } else {
        document.getElementById("DFound").innerHTML = "False"
        document.getElementById("DiscordSubData").classList.add("hidden")
    }
    if (roster_data.Found == true){
        document.getElementById("RFound").innerHTML = "True"
        document.getElementById("RCrew").innerHTML = roster_data.Crew
        document.getElementById("RRID").innerHTML = roster_data.RockstarID
        document.getElementById("RGID").innerHTML = roster_data.GuardianID
        document.getElementById("RLastVerified").innerHTML = roster_data.LastVerified
        document.getElementById("RReferBy").innerHTML = roster_data.ReferBy
        document.getElementById("RVerifiedBy").innerHTML = roster_data.VerifiedBy
        document.getElementById("RU18").innerHTML = roster_data.U18
        document.getElementById("RNotes").innerHTML = roster_data.Notes
        document.getElementById("RWarnings").innerHTML = roster_data.Warnings
    } else {
        document.getElementById("RFound").innerHTML = "False"
        document.getElementById("RosterSubData").classList.add("hidden")
    }

    if (app_data.Found == true){
        document.getElementById("AScore").innerHTML = app_data.Score
        document.getElementById("AReferal").innerHTML = app_data.Referal
        SCLink = document.getElementById("ASCLink")
        SCLink.innerHTML = app_data.SCLink
        SCLink.href = app_data.SCLink

        document.getElementById("ARegion").innerHTML = app_data.Region
        FCL = document.getElementById("AFCL")
        FCL.innerHTML = app_data.FCL
        FCL.href = app_data.FCL

        document.getElementById("ASentAt").innerHTML = app_data.SentAt
        document.getElementById("AAccepted").innerHTML = app_data.Accepted
        document.getElementById("AInvited").innerHTML = app_data.Invited
        document.getElementById("AContactedBy").innerHTML = app_data.ContactedBy
        document.getElementById("ADateContacted").innerHTML = app_data.DateContacted
    } else {
        document.getElementById("AFound").innerHTML = "False"
        document.getElementById("AppSubData").classList.add("hidden")
    }
    document.getElementById("loader").classList.add("opacity-0")
    document.getElementById("DataColumn").classList.remove("hidden")

}

function authFailed(){
    token = prompt("Auth failed, please enter token again:")
    if (token != "" && token != null) {
        TestAuth()
    }
}

function TestAuth(){
    var url = host + "/api/test";

    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);

    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Authorization", `Bearer ${token}`);

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(xhr.status);
            if (xhr.status === 401) {
                authFailed()
            } else {
                user_data = JSON.parse(xhr.responseText)
                alert(`Authed as: ${user_data.name}\nYou have the following API roles: ${user_data.roles}`)
                setCookie("token", token, 365)
            }
            
        }};

    xhr.send();
}

function ready(callback){
    // in case the document is already rendered
    if (document.readyState!='loading') callback();
    // modern browsers
    else if (document.addEventListener) document.addEventListener('DOMContentLoaded', callback);
    // IE <= 8
    else document.attachEvent('onreadystatechange', function(){
        if (document.readyState=='complete') callback();
    });
}

ready(function(){
    checkToken()

    GetMembers()
});
