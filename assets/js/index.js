function title_writer() {
    var app = document.getElementById('title_write');

    var els = document.getElementsByClassName("hide-on-load");
    Array.prototype.forEach.call(els, function(el) {
        // Do stuff here
        el.classList.add('hidden');
    });

    var typewriter = new Typewriter(app, {
        loop: false
    });
    
    function show() {
        var els = document.getElementsByClassName("animate-after-text");
        Array.prototype.forEach.call(els, function(el) {
            // Do stuff here
            el.classList.remove('opacity-0');
            el.classList.remove('h-0');
            el.classList.remove('translate-y-full');
            el.classList.add('opacity-100');
        });

        var els = document.getElementsByClassName("hide-on-load");
        Array.prototype.forEach.call(els, function(el) {
            // Do stuff here
            el.classList.remove('hidden');
        });
        
    }

    typewriter.typeString('Jakob Helgesson')
        .callFunction(() => {
            console.log('Finished typing string');
            setTimeout(show, 100);
        })
        .start();
}