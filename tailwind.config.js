console.log('env', process.env.NODE_ENV)

module.exports = {
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    content: [
      './**/*.html', 
      './*.html',
      './assets/js/*.js'
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        primary: {
          light: '#242424',
          DEFAULT: '#202020',
          dark: '#161616',
        },
      },
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
